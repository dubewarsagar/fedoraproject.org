export default {
  legacy: false,
  fallbackLocale: "en",
  fallbackWarn: false,
  missingWarn: false,
};
