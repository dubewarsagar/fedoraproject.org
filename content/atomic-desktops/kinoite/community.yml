path: community
title: Community
description: Join a community of users and contributors focused on Fedora Kinoite.
sections:
  - sectionTitle: Description
    sectionDescription:
      Fedora Kinoite is maintained by the [KDE Special Interest Group (SIG)](https://fedoraproject.org/wiki/SIGs/KDE) in Fedora and is the fruit of collaboration between the Fedora and KDE community.
      <br /><br />
      The KDE SIG is an open community seeking contributors from everywhere, and of every type. From documentation writing, to beta testing, to helping others by answering questions, there are plenty of ways to get involved and help shape the future of Kinoite!
      <br /><br />
      The KDE community creates the software and they also have a [get involved page](https://community.kde.org/Get_Involved) with a lot of helpful information on how to help.
    content: []
  - sectionTitle: Communication channels
    content:
      - title: Forum
        description: The **Fedora Kinoite** tag on Fedora's Discourse-based
          discussion forum is a great place to connect with other Fedora
          Kinoite users and contributors.
        image: public/assets/images/fedora-discussion-plus-icon.png
        link:
          text: Visit Now
          url: https://discussion.fedoraproject.org/tag/kinoite
      - title: Chat
        description: "You can chat with the Fedora Kinoite community via either
          Matrix or IRC: **#kinoite:fedoraproject.org** on Matrix or
          #kinoite on irc.libera.chat."
        image: public/assets/images/fedora-chat-plus-element.png
        link:
          text: Chat Now
          url: https://chat.fedoraproject.org/#/room/#kinoite:fedoraproject.org
  - sectionTitle: Ways to get involved
    content:
      - title: Developers
        description:
          Fedora Kinoite is powered by some amazing open-source projects, including
          [ostree](https://github.com/ostreedev/ostree), [rpm-ostree](https://github.com/coreos/rpm-ostree), and [flatpak](https://github.com/flatpak/flatpak). If you are
          a developer, consider helping out with one of these projects.
        image: public/assets/images/computer.svg
      - title: Writers
        description:
          Fedora Kinoite utilizes interesting, cutting-edge technology, and we need to tell more people about it!
          If you're a keen writer, contributing to the
          [Fedora Kinoite documentation](https://pagure.io/fedora-kde/kinoite-docs) will help others learn how their
          system works, and all the cool things they can do with it.
        image: public/assets/images/writing-notepad.svg
      - title: Testers
        description:
          Found something that isn't quite right? Want to propose a change to the way Kinoite works? We want to hear from you!
          Head over to the [KDE SIG issue tracker](https://pagure.io/fedora-kde/SIG/issues) and let us know.
        image: public/assets/images/bug.svg

  - sectionTitle: Fedora Publications
    content:
      - title: Fedora Community Blog
        description:
          The Community Blog provides a single source for members of the
          community to share important news, updates, and information about
          Fedora with others in the Project community.
        image: public/assets/images/community-blog.png
        link:
          text: Visit Now
          url: https://communityblog.fedoraproject.org/
      - title: Fedora Magazine
        description:
          Fedora Magazine is a website that hosts promotional articles and
          short guides contributed from the community about free/libre and
          open-source software that runs on or works with the Fedora Linux
          operating system.
        image: public/assets/images/fedora-magazine.png
        link:
          text: Visit now
          url: https://fedoramagazine.org/
      - title: Become a Fedora contributor
        description:
          "Both the Fedora Community Blog and Fedora Magazine are always
          [looking for article proposals, writers, and
          editors](https://docs.fedoraproject.org/en-US/fedora-magazine/contrib\
          uting/). "
