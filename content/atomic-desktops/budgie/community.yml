path: community
title: Community
description: Join a community of users and contributors focused on Fedora Budgie Atomic.
sections:
  - sectionTitle: Description
    sectionDescription:
      Fedora Budgie Atomic is maintained by the [Budgie Special Interest Group (SIG)](https://fedoraproject.org/wiki/SIGs/Budgie) in Fedora and is the result of collaboration between the Fedora and Budgie community.
      <br /><br />
      The Budgie SIG is an open community seeking contributors from everywhere, and of every type. From documentation writing, to beta testing, to helping others by answering questions, there are plenty of ways to get involved and help shape the future of Budgie!
      <br /><br />
      The Buddies of Budgie organization is responsible for the development of Budgie Desktop and have their own [Getting Involved](https://docs.buddiesofbudgie.org/organization/getting-involved) page detailing how you can get help their project!
    content: []
  - sectionTitle: Communication channels
    content:
      - title: Forum
        description: The **Fedora Onyx** tag on Fedora's Discourse-based
          discussion forum is a great place to connect with other Fedora
          Budgie Atomic users and contributors.
        image: public/assets/images/fedora-discussion-plus-icon.png
        link:
          text: Visit Now
          url: https://discussion.fedoraproject.org/tag/onyx
      - title: Chat
        description: "You can chat with the Fedora Budgie community on Matrix at **#budgie:fedoraproject.org**"
        image: public/assets/images/fedora-chat-plus-element.png
        link:
          text: Chat Now
          url: https://chat.fedoraproject.org/#/room/#budgie:fedoraproject.org
  - sectionTitle: Ways to get involved
    content:
      - title: Developers
        description:
          Fedora Budgie Atomic is powered by some amazing open-source projects, including
          [ostree](https://github.com/ostreedev/ostree), [rpm-ostree](https://github.com/coreos/rpm-ostree), and [flatpak](https://github.com/flatpak/flatpak). If you are
          a developer, consider helping out with one of these projects.
        image: public/assets/images/computer.svg
      - title: Testers
        description:
          Found something that isn't quite right? Want to propose a change to the way Budgie Atomic works? We want to hear from you!
          Head over to the [Budgie SIG issue tracker](https://pagure.io/fedora-budgie/project/issues) and let us know.
        image: public/assets/images/bug.svg

  - sectionTitle: Fedora Publications
    content:
      - title: Fedora Community Blog
        description:
          The Community Blog provides a single source for members of the
          community to share important news, updates, and information about
          Fedora with others in the Project community.
        image: public/assets/images/community-blog.png
        link:
          text: Visit Now
          url: https://communityblog.fedoraproject.org/
      - title: Fedora Magazine
        description:
          Fedora Magazine is a website that hosts promotional articles and
          short guides contributed from the community about free/libre and
          open-source software that runs on or works with the Fedora Linux
          operating system.
        image: public/assets/images/fedora-magazine.png
        link:
          text: Visit now
          url: https://fedoramagazine.org/
      - title: Become a Fedora contributor
        description:
          "Both the Fedora Community Blog and Fedora Magazine are always
          [looking for article proposals, writers, and
          editors](https://docs.fedoraproject.org/en-US/fedora-magazine/contrib\
          uting/). "
