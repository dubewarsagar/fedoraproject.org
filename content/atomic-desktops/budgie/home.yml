title: Fedora Budgie Atomic
leadin: The Budgie desktop experience in an atomic fashion.
description: Fedora Budgie Atomic ships the popular Budgie desktop environment in an atomic fashion. This enables you to surf the web, play games, enjoy music and video, manage files, and be productive without having to worry about breaking your system.

links:
  - text: Download Now
    url: /atomic-desktops/budgie/download/
header_images:
  - image: assets/images/atomic-desktops/laptop.svg
    alt_text: Screenshot of blank laptop as background to Budgie desktop
  - image: assets/images/atomic-desktops/budgie-desktop.jpg
    alt_text: Screenshot of Budgie Desktop
sections:
  - sectionTitle: Why Fedora Budgie Atomic?
    content:
      - title: Reliable
        description: Each version is updated for approximately 13 months, and each update takes effect on your next reboot, keeping your system consistent. You can even keep working while the updates are being applied!
      - title: Atomic
        description: The whole system is updated in one go, and an update will not apply if anything goes wrong, meaning you will <i>always</i> have a working computer.
      - title: Safe
        description: A previous version of your system is always kept around, just in case. If you need to go back in time, you can!
      - title: Containerized
        description: Graphical applications are installed via Flatpak, and keep themselves separate from the base system. They also allow for fine-grained control over their permissions.
      - title: Developer-Friendly
        description: Toolbx keeps all of your development tools neatly organized per-project. Keep multiple versions of your tools independent from each other and unaffected by changes to the base system.
      - title: Private, Trusted, Open Source
        description: There are no ads, and all your data belongs to you! Fedora is built on the latest open source technologies, and backed by Red Hat.
  - sectionTitle: Built On Top of the Best
    content:
      - title: Budgie Desktop
        description: A feature-rich, modern desktop designed to keep out the way of the user.
        image: public/assets/images/atomic-desktops/budgie-desktop-square.jpg
      - title: rpm-ostree
        description: rpm-ostree is a hybrid image/package system. It combines libostree and libdnf to provide atomic and safe upgrades with local RPM package layering.
        image: public/assets/images/atomic-desktops/rpm-ostree-square.png
      - title: Applications as Flatpaks
        description: Download thousands of open-source and proprietary, containerized applications thanks to Flatpak.
        image: public/assets/images/atomic-desktops/flatpak-apps-faded-square.png
      - title: Toolbx for Development
        description: Easily create OCI containers which are fully-integrated with your host system, enabling seamless usage of both GUI and CLI tools.
        image: public/assets/images/atomic-desktops/toolbox-square.png
      - title: Fedora Community & Support
        description: Fedora creates an innovative, free and open source platform for hardware, clouds, and containers that enables software developers and community members to build tailored solutions for their users.
        image: public/assets/images/atomic-desktops/fedora-square.png
  - sectionTitle: Related Projects
    content:
      - title:
        description: An atomic desktop operating system featuring the modern and elegant GNOME Desktop
        image: public/assets/images/fedora-silverblue-light.png
        url: /atomic-desktops/silverblue
      - title:
        description: An atomic desktop operating system featuring the modern and modular Plasma Desktop
        image: public/assets/images/fedora-kinoite-light.png
        url: /atomic-desktops/kinoite
      - title:
        description: An atomic desktop operating system featuring the keyboard-driven Sway window manager.
        image: public/assets/images/fedora-sericea-light.png
        url: /atomic-desktops/sway
      - title:
        description: Fedora CoreOS is an automatically updating, minimal, monolithic, container-focused operating system, designed for clusters but also operable standalone, optimized for Kubernetes but also great without it.
        image: public/assets/images/coreos-logo-light.png
        url: /coreos
